package eu.iescities.loggingrest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



// Need to change names, but this is just a very basic start
// To build *mvn compile*
// To run: *mvn exec:java*

// Need to handle both app and datastore logging
// Use /log/app for application level
//     /log/data for datastore logging



@Path("/log/app")
public class AppLogging {

    String last;
    Logger logger;

    // A new MyResource object is created everytime the API is called
    // May as well create logger on every call below
    public AppLogging () {

	logger = LoggerFactory.getLogger(AppLogging.class);
    }

    // This will query the logging system
    @GET @Path("/get")
	@Produces("application/json")
    // Usage: log/get?app=APP&level=LEVEL
	public String getlog(@QueryParam("app") String app,
			     @QueryParam("level") String level){
        logger.info ("Requesting logging for app '" + app + "' at level [" + level + "]");  
	return "Done";
    }

    // Should really be a post, though
    @POST 
	@Path("/stamp/{appid}/{level}/{timestamp}/{message}")
	@Consumes("application/json")
	public String poster(@PathParam("appid") String app,
			     @PathParam("level") String level,
			     @PathParam("message") String message,
			     @PathParam("timestamp") String timestamp
			    			     ){
	logger.info (timestamp + ": Logging a timestamped message for app '" + app + 
		     "' at level [" + level + "]: " + message + "\n");  	
	return "Done";
    }
}
