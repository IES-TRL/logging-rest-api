package eu.iescities.loggingrest;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.io.FileInputStream;

import java.net.URI;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Main class.
 *
 */
public class Main {
    
    /**     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in com.example package

	// Load base URI from properties file
	Properties prop = new Properties();
	String server, port, basedir;
	String BASE_URI;

	try {
	    //load a properties file from class path, inside static method
	    //prop.load(Main.class.getClassLoader().getResourceAsStream("logging.properties"));
	    // Load from /etc since we can make that server dependent
	    prop.load(new FileInputStream("/etc/restlogger.conf"));
	
	    server = prop.getProperty("server");
	    port = prop.getProperty("port");
	    basedir = prop.getProperty("basedir");
	    
	} catch (IOException ex) {
	    System.out.println("Assuming localhost testing server");
	    server = "localhost";
	    port = "8080";
	    basedir = "myapp";
	}
	BASE_URI = "http://" + server + ":" + port + "/" + basedir + "/";

	System.out.println("Using URI:" + BASE_URI);
    
        final ResourceConfig rc = new ResourceConfig().packages("eu.iescities.loggingrest");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
	Logger logger = LoggerFactory.getLogger(Main.class);
        //System.out.println(String.format("Jersey app started with WADL available at "
	//      + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
	System.out.println("Hit enter to stop");

	logger.info("Hello World Logger: Hello World");

        System.in.read();
        server.stop();
    }
}

