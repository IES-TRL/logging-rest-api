package eu.iescities.loggingrest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



// Need to change names, but this is just a very basic start
// To build *mvn compile*
// To run: *mvn exec:java*

// Need to handle both app and datastore logging
// Use /log/app for application level
//     /log/data for datastore logging



@Path("/log/datastore")
public class DataStoreLogging {

    String last;
    Logger logger;

    // This is the epoc time locally
    public long serverTime() {

	long epoc = System.currentTimeMillis() / 1000L;
	return epoc;
    }

    // A new MyResource object is created everytime the API is called
    // May as well create logger on every call below
    public DataStoreLogging () {

	logger = LoggerFactory.getLogger(DataStoreLogging.class);
    }

    // This will query the logging system
    @GET @Path("/get")
	@Produces("application/json")
    // Usage: log/get?app=APP&level=LEVEL
	public String getlog(@QueryParam("app") String app,
			     @QueryParam("level") String level){
        logger.info ("Requesting logging for app '" + app + "' at level [" + level + "]");  
	return "Done";
    }


    @POST 
	@Path("/stamp/write{dsid}/{city}")
	@Consumes("application/json")
	public String posterwrite(@PathParam("dsid") String dsid,
			     @PathParam("city") String city
			    			     ){
	
	logger.info (serverTime() + ": Logging a timestamped message for datastore " + dsid + 
		     "' for city [" + city + "]: Write access\n");  	
	return "Done";
    }

    @POST 
	@Path("/stamp/read{dsid}/{city}")
	@Consumes("application/json")
	public String posterread(@PathParam("dsid") String dsid,
			     @PathParam("city") String city
			    			     ){
	
	logger.info (serverTime() + ": Logging a timestamped message for datastore " + dsid + 
		     "' for city [" + city + "]: Read access\n");  	
	return "Done";
    }
}
