Logging REST API
================

Very early test implementation that includes both the app level and
dataset level APIs.

main/java/eu/iescities/loggingrest contains the (skeleton)
implementation

`AppLogging.java` implements the `/log/app` tree of the REST interface
with: 

```
/log/app/get?app=APPID&level=LEVEL
```

Retrieve the logs for specific application at the requested level. 
(The plan to replace this with a more detailed query string.)

```
/log/app/stamp/{APPID}/{level}/{timestamp}/{message}
```

Create a timestamped log message for app APPID. The timestamp is
supplied from the client end, to permit timestamping to happen
when the request is initiated in the client.


`DataStoreLogging.java` allows accesses to the dataset for the datasets with 

```
/log/datastore/get?dsid=DSID
```

Retrieve logging info for datastore DSID. A more complex querying
mechanism needs to be implemented here.

```
/log/datastore/stamp/read/{DSID}/{city} 
``` 

```
/log/datastore/stamp/write/{DSID}/{city} 
``` 

Log a timestamped read/write access to the datastore DSID. The
timestamp is generated and applied at the server end.


The actual logging is done via
[SLF4j](http://slf4j.org/faq.html). This framework allows us to change
the actual backend logging mechanism depending on how we want to
implement this. Currently this is set up and tested using the simplest
`stdio` file logging mechanism. This is set and can be changed in the
pom.xml of the project, by updating this fragment:


```xml
	<dependency>
	  <groupId>org.slf4j</groupId>
	  <artifactId>slf4j-simple</artifactId>
	  <version>${slf4jVersion}</version>
	</dependency>
```

Running
=======

To get this running you should just need:

```sh
mvn compile exec:java
```


Configuration
=============


You can drop a /etc/restlogger.conf on the server to change some
settings, for example

-- /etc/restlogger.conf --
```
server = 127.0.0.1
port = 8080
basedir = iescities
```

Testing
=======

The code in the `test` directory is just a stub and does not currently
implement any useful testing.

